#include <iostream>
#include "src/init.hpp"  

using namespace std;

int main(){

	Init havannah;
	havannah.displaySplashScreen();
	havannah.initialize();

	havannah.launch();

	return 0;
}
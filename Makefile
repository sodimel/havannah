# var

EXEC=main
FLAGS=-std=c++11

# files
$(EXEC) : main.o src/init.o src/game.o src/case.o src/minmax.o
	g++ -o $@ $^ $(FLAGS)

main.o : main.cpp src/init.hpp
	g++ -c $< $(FLAGS)

init.o : src/init.cpp src/case.o src/game.o
	g++ -c $< $(FLAGS)

game.o : src/game.cpp src/case.o src/minmax.o src/victory.o
	g++ -c $< $(FLAGS)

victory.o : src/victory.cpp src/case.o
	g++ -c $< $(FLAGS)

case.o : src/case.cpp
	g++ -c $< $(FLAGS)

minmax.o : src/minmax.cpp src/case.o src/victory.o src/game.o
	g++ -c $< $(FLAGS)

# clean/mrproper

clean:
	rm -rf *.o src/*.o

mrproper: clean
	rm -rf $(EXEC)

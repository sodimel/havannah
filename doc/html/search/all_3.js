var searchData=
[
  ['game',['Game',['../classGame.html',1,'Game'],['../classCase.html#a4c3bf58aecfe22b7a3c5a3f9f44bc393',1,'Case::game()']]],
  ['game_2ecpp',['game.cpp',['../game_8cpp.html',1,'']]],
  ['game_2ehpp',['game.hpp',['../game_8hpp.html',1,'']]],
  ['gamemode',['gamemode',['../classGame.html#a7c770080ad50f0249f98ccd1ec10182b',1,'Game::gamemode()'],['../classInit.html#aea815f209a1213a32b2c38b660baf85c',1,'Init::gamemode()']]],
  ['getcase',['getCase',['../classCase.html#aac70593be56bd0c0f7b15bfe7726c71d',1,'Case']]],
  ['getch',['getch',['../classGame.html#a129b506b9b5ca3a4cdbd621c52f7bdb6',1,'Game']]],
  ['getinitmode',['getInitMode',['../classInit.html#ae8f512c08fed42bb992e19c0d499dc88',1,'Init']]],
  ['getnewx',['getNewX',['../classMinmax.html#af37c47c5c28ee79ad64f0834854e55f0',1,'Minmax::getNewX()'],['../classVictory.html#ac02f0fda4f1eae6b61bb3e3c2b75e746',1,'Victory::getNewX()']]],
  ['getnewy',['getNewY',['../classMinmax.html#a08f19e3e421a73bfd765b1589df2adf4',1,'Minmax::getNewY()'],['../classVictory.html#a23e85ba04884d0c290ca6ead2ab890bc',1,'Victory::getNewY()']]],
  ['getsizeboard',['getSizeBoard',['../classInit.html#a3a379e91668662a12a8811cd9fc46f51',1,'Init']]],
  ['getx',['getX',['../classMinmax.html#a8728e16d8ef2e8926a9434dcae419351',1,'Minmax']]],
  ['gety',['getY',['../classMinmax.html#ad0602413f563c603f098611eaea9e67a',1,'Minmax']]]
];

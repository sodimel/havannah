var searchData=
[
  ['savegame',['saveGame',['../classGame.html#a9f7ad0384767e46fa072891c123b7d9c',1,'Game']]],
  ['searchforbridge',['searchForBridge',['../classMinmax.html#a7eaa01c6b2abdc26a137069885df46fb',1,'Minmax::searchForBridge()'],['../classVictory.html#a74546773f05af8a66c31d7998f743736',1,'Victory::searchForBridge()']]],
  ['searchforfork',['searchForFork',['../classMinmax.html#ad6eeb027e1aa87d8a308d49a3489bd08',1,'Minmax::searchForFork()'],['../classVictory.html#a0c676e8983a3ae03b4b0214e5e8b9f6c',1,'Victory::searchForFork()']]],
  ['searchforring',['searchForRing',['../classMinmax.html#add9c976de085a549ab26d8fd8e85d6de',1,'Minmax::searchForRing()'],['../classVictory.html#adf009636d478778baacee0d6a1b9596b',1,'Victory::searchForRing()']]],
  ['setcase',['setCase',['../classCase.html#a85420e37ddf6b388b82ef357b53156a2',1,'Case']]],
  ['shouldwevisitthiscase',['ShouldWeVisitThisCase',['../classMinmax.html#afdf4c184e9762102a1a459b6ef266e7f',1,'Minmax::ShouldWeVisitThisCase()'],['../classVictory.html#a8d5b75de500c97c69dd26c6333530341',1,'Victory::ShouldWeVisitThisCase()']]],
  ['shouldwevisitthiscasetoreachaborder',['ShouldWeVisitThisCaseToReachABorder',['../classMinmax.html#a8faf1bbe09a377a4fe88ddfdace605b5',1,'Minmax::ShouldWeVisitThisCaseToReachABorder()'],['../classVictory.html#a249d69c03f4d5346863c275870a58f54',1,'Victory::ShouldWeVisitThisCaseToReachABorder()']]],
  ['size',['size',['../classGame.html#a4de2bde54bebcdd189cecccc99c23557',1,'Game::size()'],['../classInit.html#a44cf89e01e1c3c562681c2e239bd68f4',1,'Init::size()'],['../classMinmax.html#ab245678452889b3139feb7be135ac350',1,'Minmax::size()'],['../classVictory.html#aa0b6e8a0fa0de76d58ddab11f3e1a513',1,'Victory::size()']]]
];

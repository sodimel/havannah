# havannah

## What ?
It's an havannah game.

## How ?
It's coded in C++

## Why ?
Woaw, this question is deep. It's a class-project.

## Havannah ?
Yup, see https://fr.wikipedia.org/wiki/Havannah for more informations.

## A video ?
Sure, watch this :  
![havannah working game](http://l3m.in/p/up/files/1494278995-havannah-working-game.webm)
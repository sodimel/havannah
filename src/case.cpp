#include <iostream>

#include "case.hpp"

using namespace std;

Case::Case(){
	game = 0;
}

void Case::setCase(int type, int value){
	switch(type){
		case 1:
			game = value;
			break;
		case 2:
			this->value = value;
			break;
		case 3:
			verif = value;
			break;
		default:
			cout << "Unexpected error." << endl;
			break;
	}
}

int Case::getCase(int type){
	switch(type){
		case 1:
			return game;
			break;
		case 2:
			return value;
			break;
		case 3:
			return verif;
			break;
		default:
			cout << "Unexpected error." << endl;
			return 0;
			break;
	}}
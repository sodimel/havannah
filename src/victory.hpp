#ifndef VICTORY_H
#define VICTORY_H

#include "case.hpp"

	//! Définit toutes les conditions de victoire (n'étais pas une classe mais seulement un ensemble de fonctions à la base).
	/*!
	  Victory appelle les 3 méthodes qui appellent elles-même des fonctions fonctionnant par récursivité. 
	*/
	class Victory{

		Case board[100][100]; /*!< tableau de cases */

		int size; /*!< taille d'un côté du plateau */

		int round; /*!< tour de jeu */

		public:
			//! Constructeur
			/*!
				Adresses du tableau, taille du plateau, round
			*/
			Victory(Case (&)[100][100], int, int);

			//!  Victoire par fourche
			/*!
				Appelle la méthode récursive searchForFork
			*/
			bool victoryFork();

			//!  Méthode récursive pour trouver une fourche
			/*!
				si on est sur un coin on change une valeur du tableau des coins visités, on explore les cases des alentours qui sont jouées par le même joueur
			*/
				void searchForFork(int, int, bool*, int*);

			//! Victoire par pont
			/*!
				Appelle la méthode récursive searchForBridge
			*/
			bool victoryBridge();

			//! Méthode récursive pour trouver un pont
			/*!
				on explore les cases voisines jouées par le même joueur, et si on est sur une case de type "coin" et que ce n'est pas celle passée en argument, alors on a un pont
			*/
				bool searchForBridge(int, int, int);
			
			//! Victoire par cercle
			/*!
				Appelle la méthode récursive searchForRing
			*/
				bool victoryRing(int, int);

			//! Méthode récursive pour trouver un cercle
			/*!
				on cherche à rejoindre les bords sans passer par des cases déjà visitées et par des cases qui appartiennent au joueur qui vient de jouer à partir des cases alentours à celle qui vient d'être jouée. Si on n'y arrive pas au moins une fois alors c'est qu'il y a un cercle.
			*/
					bool searchForRing(int, int, int);

			//! Méthode dégueulasse qui vérifie si on n'a pas un cercle plein
			/*!
				Voir explication "visuelle" via les commentaires dans victory.cpp
			*/
					bool victoryLittleRing(int, int);

			//! Méthode qui vérifie si la case appelante fut jouée par le joueur
					bool vVC(int, int);
				

			//! Vérification de visite de case
			/*!
				on vérifie si la case à visiter est jouée par le même joueur que la case d'avant, et qu'on n'est jamais passé dessus
			*/
			bool ShouldWeVisitThisCase(int, Case);

			//! Vérification de visite de case
			/*!
				quasiment pareil que "ShouldWeVisitThisCase", à ceci près que la case à visiter peut ne pas avoir reçu de "pion" d'un joueur
			*/
			bool ShouldWeVisitThisCaseToReachABorder(int, Case);

			//! Vérification de bord
			bool isThisAVisitedEdge(Case);

			//! Vérification de coin
			bool isThisAVisitedCorner(Case);
		

			//! Récupération d'abscisse
			int getNewX(int, int);
			//! Récupération d'ordonnée
			int getNewY(int, int);

			//! Remise à zéro des valeurs "verif" des cases du plateau
			void resetVerifValues();


			//! Affiche le plateau
			/*!
				En bleu ce sont les cases visitées par les algorithmes et qui ont mené à la victoire. A été utile pour débug, est sympa à regarder.
			*/
			void displayVictoryBoard();
			

			//! Affiche les cases
			/*!
				Permet d'alléger grandement les méthodes de type display*Board
			*/
			void displayChar(int, int);

	};

#endif
#include <iostream>

#include "minmax.hpp"
#include "victory.hpp"

using namespace std;

Minmax::Minmax(Case board[100][100], int size, int difficulty, int round){
	this->size = size;
	this->difficulty = difficulty;
	this->round = round;
	probability = 0;
	for(int i(0); i<100; i++){
		for(int j(0); j<100; j++){
			this->board[i][j] = board[i][j];
		}
	}
}

void Minmax::play(int deep){
	if(deep == difficulty){
		for(int i(1); i<2*size; i++){
			for(int j(1); j<2*size; j++){
				if(board[i][j].getCase(2) != -1 && board[i][j].getCase(1) == 0){
					board[i][j].setCase(1,round+deep);
					Victory v(board, size, round);
					if(deep%2 == 1){
						if(v.victoryRing(i,j) || v.victoryBridge() || v.victoryFork()){
							probability += 5;
							x = i;
							y = j;
							return;
						}
						else if(probability != 2){
							probability += 2;
							x = i;
							y = j;
						}
					}
					else{
						if(!v.victoryRing(i,j) || !v.victoryBridge() || !v.victoryFork()){
							probability -= 5;
							x = i;
							y = j;
							return;
						}
						else{
							probability -= 2;
							x = i;
							y = j;
						}
					}
				}
				board[i][j].setCase(1,0);
			}
		}
	}
	else{
		for(int i(1); i<2*size; i++){
			for(int j(1); j<2*size; j++){
				if(board[i][j].getCase(1) == 0){
					board[i][j].setCase(1,round+deep);
					if(deep%2 == 1){
						play(deep+1);
						if(probability == 5)
							return;
					}
					else{
						play(deep+1);
						if(probability == -2)
							return;
					}
				}
				board[i][j].setCase(1,0);
			}
		}
	}
}

int Minmax::getX(){
	return x;
}

int Minmax::getY(){
	return y;
}
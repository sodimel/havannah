#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "init.hpp"
#include "game.hpp"
#include "case.hpp"

using namespace std;

void Init::displaySplashScreen(){
	cout << "Havannah game\n \"Why can we program a complicated game like chess,\n but not a simple one like havannah?\n Think about it :)\"\n  Christian Freeling\n  ~ http://www.littlegolem.net/jsp/forum/topic2.jsp?forum=1&topic=1938" << endl;
}

void Init::initialize(){
	this->gamemode = getInitMode();
	this->size = getSizeBoard();
	if(gamemode%2 == 0){
		if(gamemode == 2)
			if(!load("src/pvsia.txt"))
				cout << "Loading aborted." << endl;
		if(gamemode == 4)
			if(!load("src/pvp.txt"))
				cout << "Loading aborted." << endl;
	}
	else
		initializeBoard();
}

int Init::getInitMode(){
	int choice(1);
	cout << "\n1 - New solo local game\n2 - Load solo local game\n3 - New 1v1 local game\n4 - Load 1v1 local game\n5 - New online game\n>";
		
	do{
		cin >> choice;
		if(!cin){ // thx http://stackoverflow.com/a/18728791
			cout << "error" << endl;
			cin.clear();
			cin.ignore(256,'\n');
			choice = -1;
		}

		if(choice < 1 || choice > 5)
			cout << "Error; type a number between 1 and 5 please.\n>";
	} while(choice < 1 || choice > 5);

	return choice;
}

int Init::getSizeBoard(){
	if(gamemode%2 == 0)
		return 0;

	int size(1);
	cout << "What is the size of the board (from 3 to 47) ?\n>";

	do{
		cin >> size;
		if(!cin){
			cin.clear();
			cin.ignore(256,'\n');
			size = 1;
		}
		if(size < 3 || size > 47)
			cout << "Error; type a number between 3 and 47 please.\n>";
	} while(size < 3 || size > 47);

	return size;
}

void Init::initializeBoard(){

	this->round = 0;

	// on inialise les cases du tableau des valeurs à -1;
	for(int i(0); i<2*size+1; i++){
		for(int j(0); j<2*size+1; j++){
			board[i][j].setCase(2,-1);
		}
	}
	
	// on initialise les valeurs particulières
	for(int i(1); i<(2*size); i++){
		for(int j(1); j<(2*size); j++){
			if(i<=size-1 && j>size-i || i>size-1 && j<3*size-i){ // si on est sur le plateau
				if((i==1 || i==2*size-1) || (j==1 || j==2*size-1 || j==size+1-i || j==3*size-1-i)){ // si on est sur un bord ou un coin
					if(((i==1 || i==size || i==2*size-1) && (j==1 || j == size || j == 2*size-1))) // si on est sur un coin
						board[i][j].setCase(2,7);
					else{ // si on est sur un bord, on initialise les bords (respectivement à 1,2,3,4,5,6)
						if(i==1)
							board[i][j].setCase(2,1);
						else if(j==2*size-1)
							board[i][j].setCase(2,2);
						else if(j==3*size-1-i)
							board[i][j].setCase(2,3);
						else if(i==2*size-1)
							board[i][j].setCase(2,4);
						else if(j==1)
							board[i][j].setCase(2,5);
						else if(j==size+1-i)
							board[i][j].setCase(2,6);
					}
				}
				else{ // sinon si on est au milieu du plateau
					board[i][j].setCase(2,0);
				}

			}
		}	
	}
	
	// on initialise les cases du plateau de jeu à 0
	for(int i(0); i<2*size+1; i++){
		for(int j(0); j<2*size+1; j++){
			board[i][j].setCase(1,0);
		}
	}
}

bool Init::load(string name){
	cout << "Opening " << name << "..." << endl;
	ifstream infile(name.c_str());

	if(!infile.is_open()){
		cout << "Error: " << name << " doesn't exist, or the program don't have the rights to open it." << endl;
		return false;
	}

	string line;
	round = 0;

	for(int i(0); getline(infile,line); i++){
		for(int i(0); line[i]!=0; i++)
			line[i] -= 48; // from ascii number to char (int)

		if(i == 0){ // config
			cout << "Loading configuration..." << endl;
			if(line[0] == 1)
				size = line[2];
			else
				size = line[3]+(10*line[2]);
			initializeBoard();
			cout << "Loading datas..." << endl;
		}
		else{
			round++;

			if(line[0] == 1){
				if(line[1] == 1)
					board[line[3]][line[5]].setCase(1,i);
				else
					board[line[3]][line[5]*10+line[6]].setCase(1,i);
			}
			else{
				if(line[1] == 1)
					board[line[3]*10+line[4]][line[6]].setCase(1,i);
				else
					board[line[3]*10+line[4]][line[6]*10+line[7]].setCase(1,i);
			}
		}

	}
	infile.close();
	cout << "\nGame successfully loaded." << endl;

	return true;
}

void Init::launch(){
	Game game;
	if(gamemode == 5)
		cout << "coming soon..." << endl;
	else{
		game.launch(gamemode, size, board, round+1);
		game.play();
	}

}
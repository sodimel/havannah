#ifndef CASE_H
#define CASE_H
	//! Case stocke les infos d'une case
	/*!
	  Il y a 3 types d'infos; le tour de jeu (détermine aussi le joueur qui a joué), la valeur de la case (si c'est un coin, un angle, une case du milieu du plateau), et la verif (qui est utilisée par la classe victory principalement pour savoir si l'algo est déjà passé par cette case).
	*/
	class Case{
		int game; /*!< tour de jeu (permettant de retrouver le joueur) */
		
		int value; /*!< position de la case sur le plateau (hors du plateau, sur un coin, sur un côté, au milieu) */
		
		int verif; /*!< valeur utilisée dans la classe victory */

		public:
			//! constructeur
			Case();
			//! sorte de surcharge du constructeur
			/*!
				contient un switch, je m'étais dit que ça serait plus clair et en fait ça n'a pas arrêté de me perdre. On y passe 1=game, 2=value et 3=verif et un autre int pour la valeur à affecter
			*/
			void setCase(int, int);
			//! getter
			/*!
				contient un switch qui déboussole un peu. Oon y passe 1=game, 2=value et 3=verif et il nous renvoie le contenu de la variable de case correspondante.
			*/
			int getCase(int);
	};

#endif

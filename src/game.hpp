#ifndef GAME_H
#define GAME_H

#include "case.hpp"

	//! Game contient tout ce qu'il faut pour faire une partie
	/*!
	  Le jeu est joué sur un plateau de 100*100 (évite l'emploi de new et de delete pour si peu de cases), le joueur est déterminé selon le tour (pair = joueur 2, impair = joueur 1).
	*/
	class Game{

		int gamemode; /*!< type de partie */

		int round; /*!< tour de jeu */

		int size; /*!< taille d'un côté du plateau */

		Case board[100][100]; /*!< le plateau */

		public:
			
			//! charge les variables de classe
			/*!
				via les variables transmisses depuis la classe init
			*/
			void launch(int, int, Case [100][100], int);
			//! affiche le plateau de jeu
			/*!
				utilise un savant mélange de for et de conditions afin d'afficher de manière hexagonale un rectangle auquel on a coupé deux extrémitées diamétralement opposées
			*/
			void displayBoard();
			//! affiche le plateau de jeu
			/*!
				affiche également la position du "pion" que le joueur peut jouer
			*/
			void displayBoard(int, int);
			//! Affiche les cases
			/*!
				Permet d'alléger grandement les méthodes displayBoard
			*/
			void displayChar(int, int);
			//! retourne le nombre de cases vides
			int nbPlayableCells();
			//! fonction de récupération d'appui de touche via le buffer
			/*!
				merci http://stackoverflow.com/a/912796 :)
			*/
			char getch();
			//! fonction qui affiche un écran vide et qui positionne le curseur en position 1;1
			/*!
				merci http://stackoverflow.com/q/4062045 :)
			*/
			void clearScr();
			//! sauvegarde la partie
			/*!
				écrit dans un fichier txt selon le mode de jeu, et quitte la partie en cours
			*/
			void saveGame();

			//! lance une partie
			/*!
				contient le gros while, et lances les fonctions permettant à chacun de jouer, puis vérifie si la partie est terminée
			*/
			bool play();
			//! tour de jeu d'un joueur
			bool playRound(int);
			//! tour de jeu de l'IA
			bool playAI();

			//! retourne le statut de la partie (true=terminé, false=continuer)
			bool victory(int, int);
	};

#endif

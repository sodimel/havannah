#ifndef INIT_H
#define INIT_H
	
	#include "case.hpp"

	//! Gère la création/le chargement d'une partie
	/*!
	  Affiche également l'écran d'accueil, avec la petite citation rigolote et philosophique qui -me semblait-il quand je l'ai ajoutée- était de circonstance.
	*/
	class Init{
		int gamemode; /*!< type de partie */

		int size; /*!< taille d'un côté du plateau */

		int round; /*!< tour de jeu */

		Case board[100][100]; /*!< le plateau */


		public:
			//! affiche l'écran de départ
			/*!
				le lien renvoie vers le topic du forum où a été posté le message de M. Freeling.
			*/
			void displaySplashScreen();

			//! crée/charge une partie
			void initialize();
			//! récupère le mode de jeu
				int getInitMode();
			//! récupère la taille d'un côté
				int getSizeBoard();
			
			//! crée le plateau de jeu
			/*!
				Utilise plein de conditions pour trouver les coins/bords du plateau
			*/
			void initializeBoard();

			//! charge une partie
			/*!
				le parser lit la première ligne qui contient deux variables: taille en charactères de la seconde variable, et taille du tableau  
				Il lit ensuite chaque ligne comme un nouveau tour, la première var est [taille seconde var][taille troisième var], et les deux autres variables sont l'abscisse et l'ordonnée de la case jouée
			*/
			bool load(std::string);

			//! transmet les variables à la classe Game et lance la partie
			void launch();
	};

#endif

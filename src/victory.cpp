#include <iostream>

#include "case.hpp"

using namespace std;

Victory::Victory(Case (&board)[100][100], int size, int round){
	this->size = size;
	this->round = round;
	for(int i(0); i<100; i++){
		for(int j(0); j<100; j++){
			this->board[i][j] = board[i][j];
		}
	}
}

bool Victory::victoryRing(int x, int y){
	if(round < 10)// impossible if less than 10 turns were played
		return false;

	if(round > 10 && victoryLittleRing(x, y)) // special case, impossible if less than 11 turns were played
		return true;

	int nX, nY;
	for(int i(0); i<6; i++){
		nX = getNewX(i, x);
		nY = getNewY(i, y);
		if(ShouldWeVisitThisCaseToReachABorder(board[x][y].getCase(1), board[nX][nY])){
			if(searchForRing(nX, nY, board[x][y].getCase(1))){
				displayVictoryBoard();
				return true;
			}
			resetVerifValues();
		}
	}
	return false;
}

bool Victory::searchForRing(int x, int y, int oldValue){
	int nX, nY;
	board[x][y].setCase(3,1);
	if(board[x][y].getCase(2) > 0){
		return false;
	}
	for(int i(0); i<6; i++){
		nX = getNewX(i, x);
		nY = getNewY(i, y);
		if(ShouldWeVisitThisCaseToReachABorder(oldValue, board[nX][nY])){
			if(!searchForRing(nX, nY, oldValue))
				return false;
		}
	}
	return true;
}

bool Victory::victoryLittleRing(int x, int y){ /* verify if it is a perfect circle (2*2*2*2*2*2) with a filled center
ex (* = the position of the last turn, + = cases who belongs to the same player):

  *+ |  +* |   ++ |   ++ |   ++ |   ++
 +++ | +++ |  *++ |  ++* |  +++ |  +++
 ++  | ++  |  ++  |  ++  |  *+  |  +* 

*/
	return (vVC(x, y+1) && vVC(x+1,y-1) && vVC(x+1, y) && vVC(x+1, y+1) &&  vVC(x+2, y-1) && vVC(x+2, y))||(vVC(x,y-1) && vVC(x+1,y) && vVC(x+1,y-1) && vVC(x+1,y-2) && vVC(x+2,y-1) && vVC(x+2,y-2))||(vVC(x-1,y) && vVC(x-1,y-1) && vVC(x,y-2) && vVC(x,y-1) && vVC(x+1,y-2) && vVC(x+1,y-1))||(vVC(x-1,y+1) && vVC(x-1,y+2) && vVC(x,y+1) && vVC(x,y+2) && vVC(x+1,y) && vVC(x+1,y+1))||(vVC(x,y+1) && vVC(x-1,y) && vVC(x-1,y+1) && vVC(x-1,y+2) && vVC(x-2,y+1) && vVC(x-2,y+2))||(vVC(x,y-1) && vVC(x-1,y-1) && vVC(x-1,y) && vVC(x-1,y+1) && vVC(x-2,y) && vVC(x-2,y+1));
}

bool Victory::vVC(int x, int y){ // victoryVerifCell
	if(x>=1 && x<=100 && y>=1 && y<=100)
		return board[x][y].getCase(2) != -1 && board[x][y].getCase(1) != 0 && board[x][y].getCase(1)%2 == round%2;
	return false;
}

bool Victory::victoryBridge(){
	for(int i(0); i<size*2; i++){
		for(int j(0); j<size*2; j++){ // we search good cells on all the board
			if(isThisAVisitedCorner(board[i][j])){
				for(int k(0); k<6; k++){ // we explore the others near good cells
					if(ShouldWeVisitThisCase(board[i][j].getCase(1), board[getNewX(k,i)][getNewY(k,j)])){
						board[i][j].setCase(3,1);
						if(searchForBridge(getNewX(k,i), getNewY(k,j), board[i][j].getCase(1))){
							displayVictoryBoard();
							return true;
						}
						resetVerifValues();
					}
				}
			}
		}
	}
	return false;
}

bool Victory::searchForBridge(int x, int y, int numTurn){
	for(int i(0); i<6; i++){
		int nX = getNewX(i,x), nY = getNewY(i,y);
		if(ShouldWeVisitThisCase(board[x][y].getCase(1), board[nX][nY])){
			board[x][y].setCase(3,1);
			if(board[nX][nY].getCase(2)==7 && board[nX][nY].getCase(1) != 0){
				if(numTurn != board[nX][nY].getCase(1)){
					board[nX][nY].setCase(3,1);
					return true;
				}
			}
			if(searchForBridge(nX, nY, numTurn))
				return true;
		}
	}
	board[x][y].setCase(3,0);
	return false;
}



bool Victory::victoryFork(){
	int corner[6] = {1, 2, 3, 4, 5, 6};
	bool visitedEdge[6] = {false, false, false, false, false, false};
	// on a les cêtés, on cherche si un pattern de cases reliées passe par ces côtés.
	for(int i(0); i<size*2; i++){
		for(int j(0); j<size*2; j++){ // we search good cells on all the board
			if(isThisAVisitedEdge(board[i][j])){
				for(int k(0); k<6; k++){
					if(board[i][j].getCase(2) == corner[k])
						visitedEdge[k] = true;
				}
				for(int k(0); k<6; k++){ // we explore the others cells near our cell
					if(ShouldWeVisitThisCase(board[i][j].getCase(1), board[getNewX(k,i)][getNewY(k,j)])){
						board[i][j].setCase(3,1);
							searchForFork(getNewX(k,i), getNewY(k,j), visitedEdge, corner);
						// on a les visitedCorners, on vérifie si on en a 3
						for(int i(0), j(0); i<6; i++){
							if(visitedEdge[i])
								j++;
							if(j>2){ // si on en a 3 c'est une fourche, banco
								displayVictoryBoard();
								return true;
							}
						}
					}
				}
				resetVerifValues();
				for(int i(0); i<6; visitedEdge[i] = false, i++);
			}
		}
	}
	return false;
}

void Victory::searchForFork(int x, int y, bool * visitedEdge, int * corner){
	for(int i(0); i<6; i++){
		if(board[x][y].getCase(2) == corner[i]){
			board[x][y].setCase(3,1);
			visitedEdge[i] = true;
		}
		int nX = getNewX(i,x), nY = getNewY(i,y);
		if(ShouldWeVisitThisCase(board[x][y].getCase(1), board[nX][nY])){
			board[x][y].setCase(3,1);
			searchForFork(nX, nY, visitedEdge, corner);
		}
	}	
}

bool Victory::ShouldWeVisitThisCase(int one, Case two){
	if(two.getCase(2) != -1 && two.getCase(1) != 0 && one%2 == two.getCase(1)%2 && two.getCase(3) != 1)
		return true;
	return false;
}

bool Victory::ShouldWeVisitThisCaseToReachABorder(int one, Case two){
	if(two.getCase(2) != -1 && (two.getCase(1) == 0 || two.getCase(1)%2 != one%2) && two.getCase(3) != 1)
		return true;
	return false;
}

bool Victory::isThisAVisitedEdge(Case cell){
	if(cell.getCase(2)>0 && cell.getCase(2)<7 && cell.getCase(1) != 0)
		return true;
	return false;
}

bool Victory::isThisAVisitedCorner(Case cell){
	if(cell.getCase(2) == 7 && cell.getCase(1) != 0)
		return true;
	return false;
}

int Victory::getNewX(int direction, int oldX){
	switch(direction){
		case 0: // left
			return oldX;
		break;
		case 1: // left up
			return oldX-1;
		break;
		case 2: // right up
			return oldX-1;
		break;
		case 3: // right
			return oldX;
		break;
		case 4: // right down
			return oldX+1;
		break;
		case 5: // left down
			return oldX+1;
		break;
	}
}

int Victory::getNewY(int direction, int oldY){
	switch(direction){
		case 0: // left
			return oldY-1;
		break;
		case 1: // left up
			return oldY;
		break;
		case 2: // right up
			return oldY+1;
		break;
		case 3: // right
			return oldY+1;
		break;
		case 4: // right down
			return oldY;
		break;
		case 5: // left down
			return oldY-1;
		break;
	}
}

void Victory::resetVerifValues(){
	for(int i(0); i<2*size; i++)
		for(int j(0); j<2*size; j++)
			board[i][j].setCase(3,0);
}

void Victory::displayVictoryBoard(){

	for(int i(1); i<=(size*2)-1; i++){
		for(int j(0); j<=size*2; j++){
			if((i<=size && j==2*size-i-(size-1))||(j==1 && i>size)){ // affichage x
				if(size*2-1>9 && i<10)
					cout << " " << i << " ";
				else
					cout << i << " ";
			}

			if(board[i][j].getCase(3) != 0){
				cout << "\e[44m0 \e[49m";
			}
			else
				displayChar(board[i][j].getCase(2), board[i][j].getCase(1));

			if(i>size && j == (size*3)-i) // affichage y
				cout << (size*3)-i;

		}
		cout << endl;
		if(i>=size-1 && i<2*size-1){
			for(int k(size-1); k<i; k++){
				cout << " ";
			}
		}
	}

	for(int i(0); i<=size+1; i++) // affichage y
		cout << " ";
	if(2*size-1 > 9)
		cout << "  ";
	else
		cout << " ";
	
	for(int i(0); i<size; i++){
		cout << i+1;
		if(i+1 < 10)
			cout << " ";
	}
	cout << endl;	
}

void Victory::displayChar(int value, int game){

	if(value != -1){
		if(value == 7) // angle
			cout << "\e[48;5;235m";
		else if(value > 0 && value < 7) // côté
			cout << "\e[48;5;237m";
		else // case normale
			cout << "\e[48;5;239m";
	}

	if(game != 0){ // player cell
		if(game%2 == 0)
			cout << "\e[32m";
		else
			cout << "\e[35m";
		cout << "0 \e[39m\e[49m";
	}
	else if(value != -1){
		if(value == 7) // angle
			cout << "ο";
		else if(value > 0 && value < 7) // côté
			cout << "•";
		else // case normale
			cout << "⋅";
		cout << " \e[49m";
	}
	else{
		cout << " ";
	}
}
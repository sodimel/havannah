#include <iostream>
#include <fstream>
#include <unistd.h>
#include <termios.h>
#include <string>

#include "game.hpp"
#include "case.hpp"
#include "minmax.hpp"
#include "victory.hpp"
#include "victory.cpp"

using namespace std;

void Game::launch(int gamemode, int size, Case board[100][100], int round){
	cout << "Launching game..." << endl;
	this->round = round;
	this->gamemode = gamemode;
	this->size = size;
	for(int i(0); i < 100; i++)
		for(int j(0); j < 100; j++)
			this->board[i][j] = board[i][j];
}

void Game::displayBoard(){
	for(int i(1); i<=(size*2)-1; i++){
		for(int j(0); j<=size*2; j++){
			if((i<=size && j==2*size-i-(size-1))||(j==1 && i>size)){ // affichage x
				if(size*2-1>9 && i<10)
					cout << " " << i << " ";
				else
					cout << i << " ";
			}
			
			displayChar(board[i][j].getCase(2), board[i][j].getCase(1));

			if(i>size && j == (size*3)-i) // affichage y
				cout << (size*3)-i;

		}
		cout << endl;
		if(i>=size-1 && i<2*size-1){
			for(int k(size-1); k<i; k++){
				cout << " ";
			}
		}
	}

	for(int i(0); i<=size+1; i++) // affichage y
		cout << " ";
	if(2*size-1 > 9)
		cout << "  ";
	else
		cout << " ";
	
	for(int i(0); i<size; i++){
		cout << i+1;
		if(i+1 < 10)
			cout << " ";
	}
	cout << endl;	
}

void Game::displayBoard(int x, int y){
	for(int i(1); i<=(size*2)-1; i++){
		for(int j(0); j<=size*2; j++){
			if((i<=size && j==2*size-i-(size-1))||(j==1 && i>size)){ // affichage x
				if(size*2-1>9 && i<10)
					cout << " " << i << " ";
				else
					cout << i << " ";
			}

			if(x==i && y==j)
				cout << "\e[47mO \e[49m";
			else
				displayChar(board[i][j].getCase(2), board[i][j].getCase(1));

			if(i>size && j == (size*3)-i) // affichage y
				cout << (size*3)-i;

		}
		cout << endl;
		if(i>=size-1 && i<2*size-1){
			for(int k(size-1); k<i; k++){
				cout << " ";
			}
		}
	}

	for(int i(0); i<=size+1; i++) // affichage y
		cout << " ";
	if(2*size-1 > 9)
		cout << "  ";
	else
		cout << " ";
	
	for(int i(0); i<size; i++){
		cout << i+1;
		if(i+1 < 10)
			cout << " ";
	}
	cout << endl;	
}

void Game::displayChar(int value, int game){

	if(value != -1){
		if(value == 7) // angle
			cout << "\e[48;5;235m";
		else if(value > 0 && value < 7) // côté
			cout << "\e[48;5;237m";
		else // case normale
			cout << "\e[48;5;239m";
	}

	if(game != 0){ // player cell
		if(game%2 == 0)
			cout << "\e[32m";
		else
			cout << "\e[35m";
		cout << "0 \e[39m\e[49m";
	}
	else if(value != -1){
		if(value == 7) // angle
			cout << "ο";
		else if(value > 0 && value < 7) // côté
			cout << "•";
		else // case normale
			cout << "⋅";
		cout << " \e[49m";
	}
	else{
		cout << " ";
	}
}

bool Game::play(){
	cout << "Game loaded successfully. Starting game..." << endl;

	while(round <= nbPlayableCells()){
		clearScr();
		int x((size*2-1)/2), y((size*2-1)/2);
		displayBoard(x, y);
		
		if(gamemode > 2){
			cout << "round " << round << ", player " << (round+1)%2+1 << " (use zqsd to move, enter to validate and e to save the game)" << endl;
			if(round == 2)
				cout << "Player 2, you can use the pie rule now. Let the Player 1 play now to validate the rule (you become Player 1 and Player 1 become Player 2)." << endl;

			if(playRound((round+1)%2+1))
				return true;
		}
		else{
			if((round+1)%2+1 == 1){
				cout << "round " << round << ", player " << (round+1)%2+1 << " (use zqsd to move, enter to validate and e to save the game)" << endl;
				if(playRound((round+1)%2+1))
					return true;
			}
			else{
				cout << "the AI is thinking..." << endl;
				if(playAI())
					return true;
			}
		}
			round++;
	}
	return false;
}

bool Game::playAI(){
	Minmax robotPlayer(board, size, 3, round);
	robotPlayer.play(0);
	int x = robotPlayer.getX();
	int y = robotPlayer.getY();
	board[x][y].setCase(1, round);
	
	return victory(x,y);
}

bool Game::victory(int x, int y){
	Victory v(board, size, round);

	if(v.victoryRing(x,y)){
		cout << "VICTORY RING !" << endl;
		return true;
	}
	else if(v.victoryBridge()){
		cout << "VICTORY BRIDGE !" << endl;
		return true;
	}
	else if(v.victoryFork()){
		cout << "VICTORY FORK !" << endl;
		return true;
	}
	else
		return false;	
}

int Game::nbPlayableCells(){
	int nbPlayableCells = (size*2)-1;
	int value = size*2;
	for(int i(1); i<size; i++){
		nbPlayableCells+=value;
		value+=2;
	}
	return nbPlayableCells;
}

bool Game::playRound(int player){
	int x((size*2-1)/2), y((size*2-1)/2);
	bool valid = false;

	do{
		char action = getch();

		if(action == 'z'){
			if(board[x-1][y].getCase(2) != -1)
				x--;
		}
		else if(action == 's'){
			if(board[x+1][y].getCase(2) != -1)
				x++;
		}
		else if(action == 'q'){
			if(board[x][y-1].getCase(2) != -1)
				y--;
		}
		else if(action == 'd'){
			if(board[x][y+1].getCase(2) != -1)
				y++;
		}
		else if(action == 'e'){
			saveGame();
			return true;
		}

		clearScr();
		displayBoard(x,y);
		cout << "[player " <<  player << "] [x= " << x << "][y= " << y << "]" << endl;
		if(round == 2)
			cout << "Player 2, you can use the pie rule now. Let the Player 1 play now to validate the rule (you become Player 1 and Player 1 become Player 2)." << endl;

		if(action == '\n'){
			if(board[x][y].getCase(1) != 0 || board[x][y].getCase(2) == -1)
				cout << "Vous ne pouvez pas jouer ceci. Réessayez." << endl;
			else
				valid = true;

		}
	}while(!valid);

	board[x][y].setCase(1,round);

	return victory(x, y);

}

/*
	The following function was found in stackoverflow
	--> http://stackoverflow.com/a/912796
*/
char Game::getch(){
	char buf = 0;
	struct termios old = {0};
	if(tcgetattr(0, &old) < 0)
		perror("tcsetattr()");
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	if(tcsetattr(0, TCSANOW, &old) < 0)
		perror("tcsetattr ICANON");
	if(read(0, &buf, 1) < 0)
		perror ("read()");
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if(tcsetattr(0, TCSADRAIN, &old) < 0)
		perror ("tcsetattr ~ICANON");
	return (buf);
}

void Game::clearScr(){
	cout << "\033[2J\033[1;1H"; // thx http://stackoverflow.com/q/4062045
}

void Game::saveGame(){
	cout << "saving game..." << endl;
	int x, y;
	string content;
	if(gamemode > 2)
		content = "src/pvp.txt";
	else
		content = "src/pvsia.txt";
	ofstream fichier(content.c_str(), ios::out | ios::trunc);
		cout << "file opened (and truncated)" << endl;
		if(fichier)
		{
			if(size<10)
				fichier << "1 " << size << endl;
			else
				fichier << "2 " << size << endl;

			int nb(1);
			while(nb<round){
				for(int i(1); i<2*size; i++){
					for(int j(1); j<2*size; j++){
						if(board[i][j].getCase(1) == nb){
							if(i<10)
								fichier << 1;
							else if(i<100)
								fichier << 2;
							else
								fichier << 3;

							if(j<10)
								fichier << 1;
							else if(j<100)
								fichier << 2;
							else
								fichier << 3;

							fichier << " " << i << " " << j << endl;
							nb++;
						}
					}
				}
			}
			fichier.close();
		}
		else
			cout << "L'ouverture du fichier pvp.txt a échoué." << endl;
	cout << "saving complete!" << endl;
}
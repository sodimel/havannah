#ifndef MINMAX_H
#define MINMAX_H

#include "case.hpp"

	/*!
	  Tentative un peu ratée pour gérer l'IA en mode vsIA	  
	*/
	class Minmax{

		Case board[100][100]; /*!< le plateau */
		
		int round; /*!< tour de jeu */
		
		int size; /*!< taille d'un côté du plateau */

		int difficulty; /*!< difficulté, la profondeur à laquelle l'algo est sensé aller */
		
		int probability; /*!< si oui ou non l'algo aurait intérêt à aller dans telle ou telle branche. C'est mal exploité */

		int x; /*!< position abscisse de l'explorateur */

		int y; /*!< position ordonnée de l'explorateur */

		public:
			//! Constructeur
			Minmax(Case [100][100], int, int, int);

			//! joue un tour de jeu de l'IA
			/*!
				On descend regarder un peu si il faut joueur sur la case sur laquelle on est, puis on remonte et on redescend à côté voir si c'est pas mieux. Et normalement ça permet de choisir le mieux possible le coup à jouer.
			*/
			void play(int);
			//! un getter
			int getX();
			//! un getter
			int getY();

			bool victory(int, int);
				bool victoryFork();
					void searchForFork(int, int, bool*, int*);
				bool victoryBridge();
					bool searchForBridge(int, int, int);
				bool victoryRing(int, int);
					bool searchForRing(int, int, int);
					bool victoryLittleRing(int, int);
					bool vVC(int, int);
				
			bool ShouldWeVisitThisCase(int, Case);
			bool ShouldWeVisitThisCaseToReachABorder(int, Case);
			bool isThisAVisitedEdge(Case);
			bool isThisAVisitedCorner(Case);
		
			int getNewX(int, int);
			int getNewY(int, int);
			void resetVerifValues();
		
	};

#endif
